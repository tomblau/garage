from garage.tf.baselines import GaussianMLPBaseline
from garage.experiment import LocalRunner, run_experiment
from garage.tf.algos import TRPO, PPO
from garage.envs import MountaincarEnv, normalize
from garage.envs.vrep.jaco_reaching_env import JacoReachingEnv
from garage.tf.envs import TfEnv
from garage.tf.policies import GaussianMLPPolicy
from garage.tf.core.network import ConvMergeNetwork
import garage.tf.core.layers as L
import garage.tf.core.nonlinearities as NL
import numpy as np
import tensorflow as tf
import joblib

init_filepath = None
init_filepath = "/garage/data/local/experiment/networks/trained_reaching.pkl"



def run_task(*_):
    with LocalRunner() as runner:
        env = TfEnv(JacoReachingEnv())
        if init_filepath:
            print("loading model from %s" % init_filepath)
            data = joblib.load(init_filepath)
            policy = data['policy']
            baseline = data['baseline']
        else:
            print("creating new model")
            shape = (128, 128, 4)
            mu = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=6,
                hidden_sizes=(),
                base_hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 3, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256, 64),
                hidden_nonlinearity=tf.nn.relu,
                output_nonlinearity=tf.identity,
                pool_sizes=(2, 2, 2, 2),

            )
            sigmoid = NL.ScaledShiftedSigmoid(scale_out=-5.)
            log_sig = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=6,
                hidden_sizes=(),
                base_hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 3, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256, 64),
                hidden_nonlinearity=tf.nn.relu,
                output_nonlinearity=sigmoid,
                pool_sizes=(2, 2, 2, 2),
            )
            policy = GaussianMLPPolicy(
                name="policy",
                env_spec=env.spec,
                mean_network=mu,
                std_network=log_sig)
            v_mean = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=1,
                hidden_sizes=(),
                base_hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 3, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256, 64),
                hidden_nonlinearity=tf.nn.relu,
                output_nonlinearity=tf.nn.tanh,
                pool_sizes=(2, 2, 2, 2),
            )
            v_std = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=1,
                hidden_sizes=(),
                base_hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 3, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256, 64),
                hidden_nonlinearity=tf.nn.relu,
                output_nonlinearity=tf.nn.tanh,
                pool_sizes=(2, 2, 2, 2),
            )
            reg_args = {"mean_network": v_mean, "std_network": v_std}
            baseline = GaussianMLPBaseline(
                env_spec=env.spec, regressor_args=reg_args)
        algo = PPO(
            env=env,
            policy=policy,
            baseline=baseline,
            max_path_length=env.horizon,
            discount=0.99,
            max_kl_step=0.01,
            policy_ent_coeff=0.0,
            use_neg_logli_entropy=True,
            gae_lambda=0.95,
            lr_clip_range=0.01,
            optimizer_args=dict(
                batch_size=200,
                max_epochs=10,
            ),
            plot=False,
        )
        runner.setup(algo, env)
        runner.train(n_epochs=int(1e4), batch_size=8 * env.horizon)


run_experiment(run_task, snapshot_mode="last", seed=1)
