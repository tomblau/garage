from garage.tf.baselines import GaussianMLPBaseline
from garage.experiment import LocalRunner, run_experiment
from garage.tf.algos import TRPO, PPO
from garage.envs import MountaincarEnv, normalize
from garage.envs.vrep.jaco_reaching_env import JacoReachingEnv
from garage.tf.envs import TfEnv
from garage.tf.policies import GaussianMLPPolicy
from garage.tf.core.network import ConvMergeNetwork
import garage.tf.core.layers as L
import garage.tf.core.nonlinearities as NL
import numpy as np
import tensorflow as tf
import joblib

init_filepath = "/home/tom/garage/data/local/experiment/experiment_2019_03_26_14_53_44_0001/params.pkl"

#init_filepath = None


def run_task(*_):
    with LocalRunner() as runner:
        #sess = tf.Session()
        #with sess.as_default():
        env = TfEnv(JacoReachingEnv())
        if init_filepath:
            print("loading model from %s" % init_filepath)
            data = joblib.load(init_filepath)
            policy = data['policy']
            baseline = data['baseline']
        else:
            print("creating new model")
            shape = (128, 128, 4)
            mu = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=6,
                hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 5, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256),
                pool_sizes=(2, 2, 2, 2),
            )
            sigmoid = NL.ScaledShiftedSigmoid(scale_out=2 * np.log(0.1))
            log_sig = ConvMergeNetwork(
                #input_var=mu.input_layer.input_var,
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=6,
                hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 5, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256),
                output_nonlinearity=sigmoid,
                pool_sizes=(2, 2, 2, 2),
            )
            log_sig.input_layer.input_var = mu.input_layer.input_var
            policy = GaussianMLPPolicy(
                name="policy",
                env_spec=env.spec,
                mean_network=mu,
                std_network=log_sig)
            v = ConvMergeNetwork(
                input_shape=shape,
                extra_input_shape=(6, ),
                output_dim=1,
                hidden_sizes=(256, 256, 64),
                conv_filters=(32, 64, 64, 64),
                conv_filter_sizes=(5, 5, 3, 3),
                conv_strides=(2, 2, 1, 1),
                conv_pads=("SAME", ) * 4,
                extra_hidden_sizes=(256, 256),
                pool_sizes=(2, 2, 2, 2),
            )
            v.input_layer.input_var = mu.input_layer.input_var
            reg_args = {"mean_network": v}
            baseline = GaussianMLPBaseline(
                env_spec=env.spec, regressor_args=reg_args)
        algo = PPO(
            env=env,
            policy=policy,
            baseline=baseline,
            max_path_length=env.horizon,
            discount=0.99,
            max_kl_step=0.01,
            policy_ent_coeff=1.0,
            use_neg_logli_entropy=True,
            gae_lambda=0.95,
            lr_clip_range=0.2,
            optimizer_args=dict(
                batch_size=32,
                max_epochs=10,
            ),
            plot=False,
        )
        #snapshot = algo.get_itr_snapshot(0, None)
        #p1 = L.get_all_param_values(snapshot['policy']._l_std_param)
        #print(p1[0])
        #print(snapshot['policy'].get_action(env.reset()))
        runner.setup(algo, env)
        #snapshot = algo.get_itr_snapshot(0, None)
        #pp1 = L.get_all_param_values(snapshot['policy']._l_std_param)
        #print(snapshot['policy'].get_action(env.reset()))
        #print("\n\n\n*****\n\n\n")
        #print(pp1[0])
        runner.train(n_epochs=10, batch_size=4 * env.horizon)
        #ppp1 = L.get_all_param_values(policy._l_mean)


run_experiment(run_task, snapshot_mode="last", seed=1)
